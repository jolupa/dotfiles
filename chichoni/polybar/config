[colors]
background = #409C615A
foreground = #b3bad6
active = #40468A
inactive = #7d8295
highlight = #9C615A
error = #C16D56

[bar/wpgtk]
#monitor = ${env:MONITOR:LVDS-1}
width = 100%
height = 24
offset-y = 0%
radius = 0.0
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 0
label-line-color = #f00

border-size = 0
border-color = #00000000

padding-left = 0
padding-right = 0

module-margin-left = 0
module-margin-right = 0

font-0 = menlo:fontformat=truetype:size=8;1

modules-left = 
modules-center = date volume
modules-right =  wlan battery

tray-position = none
tray-padding = 10
tray-background = ${colors.inactive}

[module/volume]
type = internal/pulseaudio
sink = alsa_output.pci-0000_00_08.0.analog-stereo
use-ui-max = false
interval = 2

format-volume = <ramp-volume> <label-volume>
format-volume-background = ${colors.active}
format-volume-padding = 2
label-volume = %percentage%%
label-muted = MUTED
label-muted-background = ${colors.inactive}
label-muted-padding = 2

ramp-volume-0 = <     ]
ramp-volume-1 = <)    ]
ramp-volume-2 = <))   ]
ramp-volume-3 = <)))  ]
ramp-volume-4 = <)))) ]
ramp-volume-5 = <)))))]

ramp-headphones-0 = <     ]
ramp-headphones-1 = <·    ]
ramp-headphones-2 = <··   ]
ramp-headphones-3 = <···  ]
ramp-headphones-4 = <···· ]
ramp-headphones-5 = <·····]

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 99

format-charging = <ramp-capacity>
format-charging-background = ${colors.active}
format-charging-padding = 2

format-discharging = <ramp-capacity> <label-discharging>
format-discharging-background = ${colors.inactive}
format-discharging-padding = 2
label-discharging = %percentage%%

ramp-capacity-0 = [   }
ramp-capacity-1 = [|  }
ramp-capacity-2 = [|| }
ramp-capacity-3 = [|||}
 
[module/wlan]
type = internal/network
interface = wlan0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-background = ${colors.active}
format-connected-padding = 2
label-connected = %essid%

format-disconnected = <label-disconnected>
format-disconnected-background = ${colors.inactive}
format-disconnected-padding = 2
label-disconnected = %ifname%

ramp-signal-0 = ^
ramp-signal-1 = ^^
ramp-singal-2 = ^^^
ramp-signal-3 = ^^^^

[module/date]
type = internal/date
interval = 1.0
time = %H:%M | %d-%m-%Y

format-background = ${colors.active}
format-padding = 2
label = %time%

[module/spotify]
type = custom/script
exec = ~/.local/share/applications/spotify.sh %artist% - %title%
tail = true
interval = 5
format-background = ${colors.active}
format-padding = 2

; vim:ft=dosini
